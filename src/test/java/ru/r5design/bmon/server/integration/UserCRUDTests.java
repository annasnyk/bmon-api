package ru.r5design.bmon.server.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.flywaydb.test.annotation.FlywayTest;
import org.flywaydb.test.junit.FlywayTestExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.r5design.bmon.server.BmonApplication;
import ru.r5design.bmon.server.config.GlobalConfig;
import ru.r5design.bmon.server.dataform.JSONResponseWrapper;
import ru.r5design.bmon.server.dataform.UserDataJSON;
import ru.r5design.bmon.server.dataform.UsersListJSON;
import ru.r5design.bmon.server.exception.InvalidPasswordException;
import ru.r5design.bmon.server.model.User;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@FlywayTest
@SpringApplicationConfiguration(classes = BmonApplication.class)
@WebIntegrationTest(value = {"spring.profiles.active=test"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, FlywayTestExecutionListener.class})
public class UserCRUDTests extends AbstractTestNGSpringContextTests {

    CloseableHttpClient client;
    String entityUrl;
    ObjectMapper mapper;

    @Autowired
    GlobalConfig globalConfig;

    TestsAuthenticationManager authMgr;
    TypeReference<JSONResponseWrapper<UserDataJSON>> userWrapperTypeRef;
    TypeReference<JSONResponseWrapper<UsersListJSON>> userlistWrapperTypeRef;

    //TODO: Тесты на некорректные данные.

    @BeforeMethod
    public void setUp() throws Exception {
        entityUrl = globalConfig.getBaseUrl() + "/user";
        authMgr = TestsAuthenticationManager.getInstance(globalConfig.getBaseUrl());
        client = authMgr.getAdminClient();
        mapper = new ObjectMapper();
        userWrapperTypeRef = new TypeReference<JSONResponseWrapper<UserDataJSON>>() {
        };
        userlistWrapperTypeRef = new TypeReference<JSONResponseWrapper<UsersListJSON>>() {
        };
    }

    @AfterMethod
    public void tearDown() throws Exception {
        client.close();
    }

    //MB: Тестирование на корректность паролей

    private List<User> getResourceEntityList(String path) throws IOException {
        return mapper.readValue(
                getClass().getClassLoader().getResource(path),
                new TypeReference<List<User>>() {
                }
        );
    }

    private String getRequest(String path) throws IOException {
        return client.execute(
                new HttpGet(entityUrl + path),
                new BasicResponseHandler()
        );
    }

    /**
     * Проверяет идентичность данных двух сущностей (без сравнения id)
     * @return true - данные сущностей (без учёта id) идентичны, иначе - false
     */
    public boolean dataEquals(User a, User b) {
        boolean nameEquals = a.getName().equals(b.getName());
        boolean emailEquals = a.getEmail().equals(b.getEmail());
        boolean passwordEquals;
        if (a.getPasswordHash()==null || b.getPasswordHash()==null) {
            passwordEquals = true;
        }
        else {
            passwordEquals = a.getPasswordHash().equals(b.getPasswordHash());
        }
        return nameEquals && emailEquals && passwordEquals;
    }

    /**
     * Генерирует строку с параметрами сущности (для дебага тестов)
     * @param u
     */
    public void printEntity(String label, User u) {
        try {
            if (label != null) {
                System.err.println(label);
            }
            System.err.println(mapper.writeValueAsString(u));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Выводит содержимое сущностей (чтобы понимать, на чём тест сломался)
     * @param label
     * @param us
     */
    private void printEntities(String label, Iterable<User> us) {
        System.err.println(label);
        for (User u : us) {
            printEntity(null, u);
        }
    }

    private void checkPresented(User u, Iterable<User> l) {
        try {
            boolean hasItem = false;
            for (User lu : l) {
                hasItem |= dataEquals(lu, u);
                if (hasItem) {
                    break;
                }
            }
            Assert.assertTrue(hasItem, "Item \"" + u.getName() + "\" is presented");

        } catch (AssertionError e) {
            printEntity("Expected item:", u);
            printEntities("Items list:", l);
            throw e;
        }
    }

    @Test(enabled = false)
    public void list() throws IOException {
        List<User> listExpected = getResourceEntityList("integration/users/list.json");
        String body = getRequest("/list");
        JSONResponseWrapper<UsersListJSON> usersListObj = mapper.readValue(body, userlistWrapperTypeRef);
        List<User> listReal = usersListObj.getResponse().users;
        Assert.assertEquals(listReal.size(), listExpected.size(), "List size not equals expected");
        for (User u : listExpected) {
            checkPresented(u, listReal);
        }
    }

    @Test(enabled = false)
    public void get() throws IOException {
        String body = getRequest("/list");
        //MB: Поменять на findOneByEmail и вычитывать юзеров из JSON
        JSONResponseWrapper<UsersListJSON> userListObj = mapper.readValue(body, userlistWrapperTypeRef);
        Iterable<User> listExpected = userListObj.getResponse().users;
        for (User u : listExpected) {
            body = getRequest("/get/" + u.getId());
            User actual = mapper.<JSONResponseWrapper<UserDataJSON>>readValue(
                    body, userWrapperTypeRef
            ).getResponse().user;
            try {
                Assert.assertTrue(dataEquals(u, actual), "We can't get single item \"" + u.getName() + "\"");
            } catch (AssertionError e) {
                printEntity("Expected item:", u);
                printEntity("Actual item:", actual);
                throw e;
            }
        }
    }

    /**
     * Тестирует метод add, отправляя, в качестве параметров, все ноды объекта params.
     * @param params объект с парами вида ключ:значение, каждая из которых будет помещена в запрос
     * @return полученный от сервиса объект User
     */
    private User sendAdd(JsonNode params) throws IOException {
        HttpPost req = new HttpPost(entityUrl + "/add");
        LinkedList<NameValuePair> reqParams = new LinkedList<>();
        Iterator<Map.Entry<String,JsonNode>> fields = params.fields();
        while (fields.hasNext()) {
            Map.Entry<String,JsonNode> entry = fields.next();
            reqParams.add(new BasicNameValuePair(entry.getKey(),entry.getValue().asText()));
        }
        req.setEntity(new UrlEncodedFormEntity(reqParams, Charset.forName("UTF-8")));
        String body = client.execute(req, new BasicResponseHandler());
        JSONResponseWrapper<UserDataJSON> addedObj = mapper.readValue(body, userWrapperTypeRef);
        User added = addedObj.getResponse().user;
        return added;
    }

    @Test(enabled = false, dependsOnMethods = "list")
    public void add() throws IOException, URISyntaxException {
        List<User> listAdd = getResourceEntityList("integration/users/add.json");
        byte[] jsonData = Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource("integration/users/add.json").toURI()));
        JsonNode rootNode = mapper.readTree(jsonData);
        Iterator<JsonNode> nodeItr = rootNode.iterator();
        List<User> listActual = new LinkedList<>();
        while (nodeItr.hasNext()) {
            JsonNode values = nodeItr.next();
            listActual.add(sendAdd(values));
        }
        for (User u: listAdd) {
            checkPresented(u,listActual);
        }
    }

    /**
     * Посылает запрос "update" объекта /id с параметрами из списка params
     * @param id
     * @param params
     * @return
     * @throws IOException
     */
    public User sendUpdate(long id, HashMap<String,String> params) throws IOException {
        HttpPost req = new HttpPost(entityUrl + "/update");
        LinkedList<NameValuePair> reqParams = new LinkedList<>();
        Iterator<Map.Entry<String,String>> pItr = params.entrySet().iterator();
        while (pItr.hasNext()) {
            Map.Entry<String,String> entry = pItr.next();
            reqParams.add(new BasicNameValuePair(entry.getKey(),entry.getValue()));
        }
        reqParams.add(new BasicNameValuePair("id",Long.toString(id)));
        req.setEntity(new UrlEncodedFormEntity(reqParams, Charset.forName("UTF-8")));
        String body = client.execute(req, new BasicResponseHandler());
        JSONResponseWrapper<UserDataJSON> updatedObj = mapper.readValue(body, userWrapperTypeRef);
        User updated = updatedObj.getResponse().user;
        return updated;
    }

    public void checkEntityEquals (User a, User b) {
        try {
            Assert.assertTrue(dataEquals(a, b), "Entities data are not equal");
        } catch (AssertionError e) {
            printEntity("Item 1:", a);
            printEntity("Item 2:", b);
            throw e;
        }
    }

    @Test(enabled = false, dependsOnMethods = "add")
    public void update() throws IOException, InvalidPasswordException {
        final List<User> listExcluded = getResourceEntityList("integration/users/list.json");
        String body = getRequest("/list");
        JSONResponseWrapper<UsersListJSON> listObj = mapper.readValue(body, userlistWrapperTypeRef);
        List<User> listAll = listObj.getResponse().users;
        Iterator<User> addedUsers = listAll.stream().filter(
                u -> !listExcluded.stream().anyMatch(
                        eu -> dataEquals(eu,u)
                )
        ).iterator();
        //Очень коряво, без внешних файлов и зависит от порядка добавления юзеров
        //Написано в спешке
        User actual;
        HashMap<String,String> params = new HashMap<>();
        User updateNameEmail = addedUsers.next();
        updateNameEmail.setName("Другое имя юзера");
        params.put("name","Другое имя юзера");
        updateNameEmail.setEmail("anothermailbox@bmon.dev");
        params.put("email","anothermailbox@bmon.dev");
        actual = sendUpdate(updateNameEmail.getId(),params);
        checkEntityEquals(actual,updateNameEmail);
        params.clear();
        User updateEmailPassword = addedUsers.next();
        updateEmailPassword.setEmail("hurrdurr@bmon.dev");
        params.put("email","hurrdurr@bmon.dev");
        updateEmailPassword.setPassword("sunshine","stardust");
        params.put("password","sunshine");
        params.put("newpassword","stardust");
        actual = sendUpdate(updateEmailPassword.getId(),params);
        checkEntityEquals(actual,updateEmailPassword);
    }

}
