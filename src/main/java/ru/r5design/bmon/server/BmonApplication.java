package ru.r5design.bmon.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;


@SpringBootApplication
public class BmonApplication extends SpringBootServletInitializer {

    //Этот бин исключительно для генератора маппингов IDEA (закомментировать после использования)
	/*
	@Bean
	public SessionFactory sessionFactory(HibernateEntityManagerFactory hemf){
		return hemf.getSessionFactory();
	}
	//*/

    public static void main(String[] args) {
        SpringApplication.run(BmonApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(BmonApplication.class);
    }

	@Component
	private static class MyContainerCustomizer implements EmbeddedServletContainerCustomizer {

		@Override
		public void customize(ConfigurableEmbeddedServletContainer container) {
			container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/404"));
			container.addErrorPages(new ErrorPage(HttpStatus.FORBIDDEN, "/403"));
		}
	}
}
