package ru.r5design.bmon.server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.r5design.bmon.server.model.User;
import ru.r5design.bmon.server.repository.UserRepository;

/**
 * Кастомный сервис инфы о юзере, на основе сущности из БД
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository rep;

    @Override
    public User loadUserByUsername(String s) throws UsernameNotFoundException {
        User u = rep.findOneByEmail(s);
        return u;
    }

    public User loadUserById(Long id) throws UsernameNotFoundException {
        User u = rep.findOne(id);
        return u;
    }
}
