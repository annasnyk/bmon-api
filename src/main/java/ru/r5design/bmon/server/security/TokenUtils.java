package ru.r5design.bmon.server.security;

import io.jsonwebtoken.*;
import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import ru.r5design.bmon.server.config.GlobalConfig;
import ru.r5design.bmon.server.model.User;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Класс для манипуляций с JWT-токенами
 */
@Service
public class TokenUtils {

    @Autowired
    private GlobalConfig globalConfig;

    /**
     * Получает JWT-токен из запроса одним из возможных способов (Cookies, Header)
     *
     * @param request
     * @return
     */
    public String getTokenFromRequest(HttpServletRequest request) throws BadCredentialsException {
        String headerAuth = request.getHeader("Authorization");
        String cookieAuth = null;
        Cookie[] requestCookies = request.getCookies();
        if (requestCookies != null) {
            cookieAuth = Arrays.stream(
                    requestCookies
            ).filter(
                    c -> c.getName().equals("authorization")
            ).findFirst().orElse(
                    new Cookie("null", null)
            ).getValue();
            if (cookieAuth != null) {
                try {
                    cookieAuth = URLDecoder.decode(cookieAuth, "UTF-8");
                } catch (UnsupportedEncodingException ignored) {
                }
            }
        }
        String token = null;
        if (headerAuth == null || !headerAuth.startsWith("Bearer ")) {
            if (cookieAuth == null || !cookieAuth.startsWith("Bearer ")) {
                return null;
            } else {
                token = cookieAuth;
            }
        } else {
            token = headerAuth;
        }
        return token;
    }

    /**
     * Убирает токен из Cookies
     *
     * @param response
     */
    public void removeFromCookies(HttpServletResponse response) {
        Cookie c = new Cookie("authorization", "");
        c.setPath(globalConfig.getBasePath());
        c.setMaxAge(0);
        response.addCookie(c);
    }

    /**
     * Генерирует JWT
     */
    public String get(User u) {
        String base64Signature = Base64.encodeBase64String(u.getSessionKey());
        //MB: Можно ли подписывать JWT хэшем пароля?
        String jwt = "Bearer " +
                Jwts.builder()
                        .claim("id", u.getId())
                        .setIssuedAt(new Date())
                        .setExpiration(new DateTime().plusDays(30).toDate())
                        .signWith(SignatureAlgorithm.HS256, base64Signature)
                        .setHeaderParam("typ", "JWT")
                        .compact();
        return jwt;
    }

    /**
     * Добавляет токен в Cookies
     *
     * @param response
     */
    public String getAndSetInCookies(User u, HttpServletResponse response) {
        String jwt = get(u);
        Cookie c = null;
        try {
            c = new Cookie("authorization", URLEncoder.encode(jwt, "UTF-8"));
        } catch (UnsupportedEncodingException ignored) {
        }
        c.setPath(globalConfig.getBasePath());
        c.setMaxAge((int) TimeUnit.SECONDS.convert(30, TimeUnit.DAYS));
        response.addCookie(c);
        return jwt;
    }

    public Long getUserId(String token) throws BadCredentialsException {
        String unsignedPart;
        unsignedPart = token.substring((token.startsWith("Bearer ") ? "Bearer ".length() : 0), token.lastIndexOf('.') + 1);
        Claims claims = Jwts.parser().parseClaimsJwt(unsignedPart).getBody();
        if (!claims.containsKey("id")) {
            return null;
        }
        long id = (Integer) claims.get("id");
        return id;
    }

    public boolean validate(String token, byte[] signature) {
        String actualToken = token.substring((token.startsWith("Bearer ") ? "Bearer ".length() : 0));
        try {
            Jwts.parser().setSigningKey(signature).parse(actualToken);
        } catch (SignatureException | ExpiredJwtException | MalformedJwtException e) {
            return false;
        }
        return true;
    }

}
