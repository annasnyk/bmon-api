package ru.r5design.bmon.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

/**
 * Created by R5 on 12.06.2016.
 */
@Entity
@Table(name = "users_groups")
public class UserGroup {
    @JsonIgnore
    private long id;
    @JsonIgnore
    private long userId;
    @JsonIgnore
    private short groupId;
    @JsonIgnore
    private User usersByUserId;

    private String groupName;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id", nullable = false)
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) { this.userId = userId; }

    @Basic
    @Column(name = "group_id", nullable = false)
    public short getGroupId() {
        return groupId;
    }

    public void setGroupId(short groupId) {
        groupName = Group.getInstance().getName(groupId);
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserGroup userGroup = (UserGroup) o;

        if (id != userGroup.id) return false;
        if (userId != userGroup.userId) return false;
        if (groupId != userGroup.groupId) return false;

        return true;
    }

    //Иначе (если начало метода через set) Hibernate ругается
    //TODO: Понять, как это делается адекватным способом
    @JsonProperty(value = "name")
    public void nameSet(String groupName) {
        setGroupId(Group.getInstance().getId(groupName));
    }

    public String nameGet() {
        return groupName;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (int) groupId;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public User getUsersByUserId() {
        return usersByUserId;
    }

    public void setUsersByUserId(User usersByUserId) {
        this.usersByUserId = usersByUserId;
    }

}
