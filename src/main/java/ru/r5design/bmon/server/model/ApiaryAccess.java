package ru.r5design.bmon.server.model;

import javax.persistence.*;

/**
 * Created by R5 on 20.06.2016.
 */
@Entity
@Table(name = "apiaries_access")
public class ApiaryAccess {
    private Long id;
    private Long userId;
    private Long apiaryId;
    private Boolean readOnly;
    private User users;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id", nullable = false)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "apiary_id", nullable = false)
    public Long getApiaryId() {
        return apiaryId;
    }

    public void setApiaryId(Long apiaryId) {
        this.apiaryId = apiaryId;
    }

    @Basic
    @Column(name = "read_only", nullable = false)
    public Boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApiaryAccess that = (ApiaryAccess) o;

        if (id != that.id) return false;
        if (userId != that.userId) return false;
        if (apiaryId != that.apiaryId) return false;
        if (readOnly != that.readOnly) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (int) (apiaryId ^ (apiaryId >>> 32));
        result = 31 * result + (readOnly ? 1 : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public User getUser() {
        return users;
    }

    public void setUser(User users) {
        this.users = users;
    }

}
