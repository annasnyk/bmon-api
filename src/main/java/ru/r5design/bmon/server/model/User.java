package ru.r5design.bmon.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.token.SecureRandomFactoryBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import ru.r5design.bmon.server.exception.InvalidPasswordException;
import ru.r5design.bmon.server.security.NamedAuthority;

import javax.persistence.*;
import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Пользователь системы
 */
@Entity
@Table(name = "users")
public class User implements UserDetails {
    private static final short HASH_ROUNDS = 6;
    private long id;
    private String name;
    private String email;
    @JsonIgnore
    private String passwordHash;
    @JsonIgnore
    private byte[] sessionKey;
    @JsonIgnore
    private Collection<ApiaryAccess> apiariesAccesses;
    @JsonProperty(value = "groups")
    private Collection<UserGroup> usersGroupsById;

    //Костыльный метод для задания фейковых групп
    //MB: Делать всё через моки, а не вот эту шляпу мутить
    //При сохранении юзера группы сохранятся?
    @JsonSetter(value = "groups")
    void setGroups(List<String> gr) {
        if (usersGroupsById == null) {
            usersGroupsById = new ArrayList<>();
        }
        for (String name : gr) {
            UserGroup g = new UserGroup();
            g.setUserId(id);
            g.setGroupId(Group.getInstance().getId(name));
            if (!usersGroupsById.stream().anyMatch(
                    grp -> grp.nameGet().equals(name)
            )) {
                usersGroupsById.add(g);
            }
        }
    }

    @JsonProperty(value = "groups")
    public List<String> groups() {
        LinkedList<String> groups = new LinkedList<>();
        for (UserGroup g : getUsersGroupsById()) {
            groups.add(Group.getInstance().getName(g.getGroupId()));
        }
        return groups;
    }


    boolean hasGroup(String groupName) {
        for (UserGroup g : getUsersGroupsById()) {

        }
        return false;
    }

    @PrePersist
    public void preSave() {
        updateSessionKey();
    }

    public void updateSessionKey() {
        try {
            SecureRandomFactoryBean srb = new SecureRandomFactoryBean();
            SecureRandom sr = srb.getObject();
            sessionKey = new byte[16];
            sr.nextBytes(sessionKey);
        } catch (Exception e) {
            //"Exception - in case of creation errors" в srb.getObject()
            //Совершенно невнятное обяснение, почему этот эксепшен вообще может вдруг вывалиться
            e.printStackTrace();
        }
    }

    @PostPersist
    public void postSave() {
        usersGroupsById = new ArrayList<>();
        UserGroup standartGroup = new UserGroup();
        standartGroup.setGroupId(Group.getInstance().getId("user"));
        standartGroup.setUserId(id);
        usersGroupsById.add(standartGroup);
    }

    /**
     * Устанавливает новый пароль пользователя.
     * Если у пользователя уже есть пароль - необходимо указать его (иначе - можно указать равным null).
     *
     * @param oldPassword пароль пользователя в данный момент
     * @param password    новый пароль пользователя
     * @throws InvalidPasswordException неверно указан актуальный пароль пользователя (oldPassword)
     */
    @JsonProperty(value = "password", access = JsonProperty.Access.WRITE_ONLY)
    public void setPassword(String oldPassword, String password) throws InvalidPasswordException {
        if (this.passwordHash != null) {
            if (oldPassword == null || !BCrypt.checkpw(oldPassword, this.passwordHash)) {
                throw new InvalidPasswordException("Can't set new password. Supplied old password is invalid.");
            }
        }
        String pwHash = BCrypt.hashpw(password, BCrypt.gensalt(HASH_ROUNDS));
        this.passwordHash = pwHash;
    }

    /**
     * Проверяет, соответствует ли указанный пароль данному пользователю
     *
     * @param password
     * @return
     */
    public boolean checkPassword(String password) {
        return BCrypt.checkpw(password, passwordHash);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 64)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "email", nullable = false, length = 255)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 60)
    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @Basic
    @Column(name = "session", nullable = false)
    public byte[] getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(byte[] session) {
        this.sessionKey = session;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (passwordHash != null ? !passwordHash.equals(user.passwordHash) : user.passwordHash != null) return false;
        if (!Arrays.equals(sessionKey, user.sessionKey)) return false;
        if (apiariesAccesses != null ? !apiariesAccesses.equals(user.apiariesAccesses) : user.apiariesAccesses != null)
            return false;
        return usersGroupsById != null ? usersGroupsById.equals(user.usersGroupsById) : user.usersGroupsById == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (passwordHash != null ? passwordHash.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(sessionKey);
        result = 31 * result + (apiariesAccesses != null ? apiariesAccesses.hashCode() : 0);
        result = 31 * result + (usersGroupsById != null ? usersGroupsById.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "userId")
    public Collection<ApiaryAccess> getApiariesAccesses() {
        return apiariesAccesses;
    }

    public void setApiariesAccesses(Collection<ApiaryAccess> apiariesAccesses) {
        this.apiariesAccesses = apiariesAccesses;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "usersByUserId")
    @Cascade(CascadeType.ALL)
    public Collection<UserGroup> getUsersGroupsById() {
        return usersGroupsById;
    }

    public void setUsersGroupsById(Collection<UserGroup> usersGroupsById) {
        this.usersGroupsById = usersGroupsById;
    }

    //Оверрайды для Spring Security
    @JsonIgnore
    @Transient
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<UserGroup> groups = getUsersGroupsById();
        final Group groupOps = Group.getInstance();
        HashSet<Short> userGroupsIds = new HashSet<>();
        for (UserGroup g : groups) {
            userGroupsIds.addAll(groupOps.getIncludedList(g.getGroupId()));
        }
        List<NamedAuthority> authorities = userGroupsIds.stream().map(
                g -> new NamedAuthority(
                        "ROLE_" + groupOps.getName(g).toUpperCase()
                )
        )
                .collect(
                        Collectors.toCollection(LinkedList::new)
                );
        return authorities;
    }

    @JsonIgnore
    @Transient
    @Override
    public String getPassword() {
        return getPasswordHash();
    }

    /**
     * Устанавливает новый пароль, при условии что старый не указан (равен null).
     *
     * @param password
     * @throws InvalidPasswordException у пользователя уже есть пароль
     */
    public void setPassword(String password) throws InvalidPasswordException {
        setPassword(null, password);
    }

    @JsonIgnore
    @Transient
    @Override
    public String getUsername() {
        return getEmail();
    }

    @JsonIgnore
    @Transient
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Transient
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Transient
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Transient
    @Override
    public boolean isEnabled() {
        return true;
    }

    //TODO: Сама сущность выглядит как не лучшее место для реализации UserDetails. Рефакторить.
}
