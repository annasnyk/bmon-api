package ru.r5design.bmon.server.model;

import javax.persistence.*;

/**
 * Хранилище данных конкретного пользователя на клиентском устройстве
 */
@Entity
@Table(name = "storages")
public class Storage {
    private Long id;
    private Long deviceId;
    private Long userId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "device_id", nullable = false)
    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    @Basic
    @Column(name = "user_id", nullable = false)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Storage storage = (Storage) o;

        if (id != null ? !id.equals(storage.id) : storage.id != null) return false;
        if (deviceId != null ? !deviceId.equals(storage.deviceId) : storage.deviceId != null) return false;
        if (userId != null ? !userId.equals(storage.userId) : storage.userId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (deviceId != null ? deviceId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        return result;
    }
}
