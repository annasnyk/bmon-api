package ru.r5design.bmon.server.repository;

/**
 * Created by R5 on 03.06.2016.
 */

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.r5design.bmon.server.model.Storage;

@Repository
public interface StorageRepository extends CrudRepository<Storage, Long> {
}