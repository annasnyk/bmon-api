package ru.r5design.bmon.server.dataform;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.r5design.bmon.server.model.User;

/**
 * JSON-обёртка для сущности типа "Пользователь"
 */
public class UserDataJSON {

    public User user = null;

    @JsonCreator
    public UserDataJSON(@JsonProperty("user") User user) {
        this.user = user;
    }
}
