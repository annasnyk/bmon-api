package ru.r5design.bmon.server.dataform;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.r5design.bmon.server.model.User;

import java.util.List;

/**
 * JSON-обёртка для списка юзеров
 */
public class UsersListJSON {

    public List<User> users = null;

    @JsonCreator
    public UsersListJSON(@JsonProperty("users") List<User> users) {
        this.users = users;
    }
}
