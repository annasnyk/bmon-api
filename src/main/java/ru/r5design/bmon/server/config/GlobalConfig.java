package ru.r5design.bmon.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Основные параметры приложения
 */
@Component
public class GlobalConfig {

    @Value("${server.contextPath}")
    protected String basePath;

    @Value("${app.url}")
    protected String baseUrl;

    @Value("${app.changesFolder}")
    protected String changesFolder;

    @Value("${app.changesPath}")
    protected String changesPath;

    @Value("${app.mobileSqlFolder}")
    protected String mobileSqlFolder;

    @Value("${app.changesUrl}")
    protected String changesUrl;

    @Value("${app.version}")
    protected String apiVersion;

    public String getBasePath() {
        return basePath;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getChangesFolder() {
        return changesFolder;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public String getChangesUrl() {
        return changesUrl;
    }

    public String getChangesPath() {
        return changesPath;
    }

    public String getMobileSqlFolder() {
        return mobileSqlFolder;
    }
}
