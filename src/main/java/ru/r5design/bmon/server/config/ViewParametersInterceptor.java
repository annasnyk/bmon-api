package ru.r5design.bmon.server.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ru.r5design.bmon.server.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Добавляет дефолтные параметры к вьюхам
 */
@Component
public class ViewParametersInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    GlobalConfig globalConfig;

    private Map<String,Object> buildParams() {
        Map<String,Object> defaultModelParams = new HashMap();
        //Базовый путь к api
        defaultModelParams.put("BASE_URL", globalConfig.getBaseUrl());
        //Юзер, активный в данный момент
        User currentUser=null;
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal!=null && !principal.getClass().isAssignableFrom(String.class)) {
                currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            }
        }
        catch (NullPointerException e) {
        }
        defaultModelParams.put("currentUser", currentUser);
        return defaultModelParams;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (modelAndView!=null && modelAndView.hasView()) {
            modelAndView.getModelMap().addAllAttributes(buildParams());
        }
        super.postHandle(request, response, handler, modelAndView);
    }
}
