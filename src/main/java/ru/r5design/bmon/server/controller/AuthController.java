package ru.r5design.bmon.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.r5design.bmon.server.config.GlobalConfig;
import ru.r5design.bmon.server.dataform.AuthorizationTokenJSON;
import ru.r5design.bmon.server.dataform.JSONResponseWrapper;
import ru.r5design.bmon.server.model.User;
import ru.r5design.bmon.server.repository.UserRepository;
import ru.r5design.bmon.server.security.TokenUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Контроллер всяческой аутентификации
 */
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserRepository rep;

    @Autowired
    private GlobalConfig globalConfig;

    @Autowired
    private TokenUtils tokenUtils;

    /**
     * Генерирует JWT-токен авторизации.
     * @param email email пользователя
     * @param password пароль пользователя
     * @param cookie нужно ли установить Cookie с JWT клиенту, вызывающему метод
     * @return JWT-токен вида: "Bearer %токен%"
     * @throws Exception
     */
    @RequestMapping(value = "/login", method=RequestMethod.POST)
    public JSONResponseWrapper<AuthorizationTokenJSON> login(
            @RequestParam(value="email") String email,
            @RequestParam(value="redirect", required = false) String redirectUrl,
            @RequestParam(value="password") String password,
            @RequestParam(value="cookie", defaultValue = "false") boolean cookie,
            HttpServletResponse response
    ) throws Exception {
        User u = rep.findOneByEmail(email);
        if (u==null || !u.checkPassword(password)) {
            throw new BadCredentialsException("Invalid email+password combination");
        }
        AuthorizationTokenJSON tokenObj = new AuthorizationTokenJSON();
        JSONResponseWrapper<AuthorizationTokenJSON> res = new JSONResponseWrapper<>(tokenObj, globalConfig);
        String jwt = null;
        if (cookie) {
            tokenUtils.getAndSetInCookies(u, response);
            response.sendRedirect(redirectUrl==null ? globalConfig.getBasePath() : redirectUrl);
        }
        else {
            jwt = tokenUtils.get(u);
            res.getResponse().token = jwt;
        }
        return res;
    }

    /**
     * Осуществляет выход пользователя из системы.
     * @param allDevices выйти на всех устройствах.
     *                   Если установлено на "true" - все токены, связанные с пользователем, становятся невалидными.
     * @param request
     * @param response
     */
    @RequestMapping(value = "/logout", method=RequestMethod.POST)
    public void logout(
            @RequestParam(value="alldevices", required = false, defaultValue = "false") boolean allDevices,
            @RequestParam(value="redirect", required = false) String redirectUrl,
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException {
        if (allDevices) {
            String token = tokenUtils.getTokenFromRequest(request);
            User user = rep.findOne(tokenUtils.getUserId(token));
            if (user!=null) {
                user.updateSessionKey();
                rep.save(user);
            }
        }
        tokenUtils.removeFromCookies(response);
        if (redirectUrl!=null) {
            response.sendRedirect(redirectUrl);
        }
    }

}
