package ru.r5design.bmon.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.r5design.bmon.server.config.GlobalConfig;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Контроллер авторизации с выводом необходимых шаблонов
 */
@Controller
@RequestMapping("/auth")
public class AuthViewsController {

    @Autowired
    GlobalConfig globalConfig;

    @RequestMapping(value = "/login", method= RequestMethod.GET)
    public String loginPage(
            @RequestParam(value="redirect", required=false) String redirectUrl,
            Map<String, Object> model,
            HttpServletRequest request,
            HttpServletResponse response) {
        model.put("loginUrl",globalConfig.getBaseUrl()+"/auth/login");
        if (redirectUrl!=null) {
            model.put("redirectUrl", redirectUrl);
            response.setStatus(HttpStatus.FORBIDDEN.value());
        }
        return "pages/auth/login";
    }
}