package ru.r5design.bmon.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.r5design.bmon.server.config.GlobalConfig;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Обработка ошибок приложений
 */
@Controller
public class ExceptionsController {

    static final String TEMPLATES_FOLDER = "errors/";

    @Autowired
    GlobalConfig globalConfig;

    @ExceptionHandler
    @RequestMapping("/404")
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleResourceNotFoundException(Map<String, Object> model, HttpServletRequest request) {
        model.put("requestPath", request.getAttribute(RequestDispatcher.FORWARD_REQUEST_URI));
        model.put("goBackUrl", globalConfig.getBaseUrl());
        return TEMPLATES_FOLDER+"404";
    }

    @ExceptionHandler
    @RequestMapping("/403")
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void forbiddenException(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String redirect = response.getHeader("RedirectUrl");
        response.sendRedirect("/auth/login"+(redirect!=null ? "?redirect="+redirect : ""));
    }
}
