package ru.r5design.bmon.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.r5design.bmon.server.config.GlobalConfig;
import ru.r5design.bmon.server.dataform.JSONResponseWrapper;
import ru.r5design.bmon.server.dataform.UserDataJSON;
import ru.r5design.bmon.server.dataform.UsersListJSON;
import ru.r5design.bmon.server.exception.InvalidPasswordException;
import ru.r5design.bmon.server.model.User;
import ru.r5design.bmon.server.model.UserGroup;
import ru.r5design.bmon.server.repository.UserGroupRepository;
import ru.r5design.bmon.server.repository.UserRepository;
import ru.r5design.bmon.server.security.SecurityUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Контроллер для сущностей типа "Пользователь"
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository rep;

    @Autowired
    private UserGroupRepository gRep;

    @Autowired
    private GlobalConfig globalConfig;

    /**
     * Возвращает список пасек в формате JSON
     *
     * @return список сущностей типа "пользователь"
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public JSONResponseWrapper<UsersListJSON> list() {
        //Запрашиваем все группы и склеиваем их с юзерами, чтобы JPA не делал лишних запросов
        //Когда юзеры будут выбираться по критериям, надо будет запиливать в UserRepository метод list с JPQL-запросами
        List<User> users = rep.findAll();
        Iterable<UserGroup> groups = gRep.findAll();
        HashMap<Long, LinkedList> sortedGroups = new HashMap<>();
        LinkedList<UserGroup> uGs;
        for (UserGroup g : groups) {
            uGs = sortedGroups.get(g.getUserId());
            if (uGs == null) {
                sortedGroups.put(
                        g.getUserId(),
                        new LinkedList<UserGroup>() {{
                            push(g);
                        }}
                );
            } else {
                uGs.push(g);
            }
        }
        for (User u : users) {
            u.setUsersGroupsById(sortedGroups.get(u.getId()));
        }
        UsersListJSON uList = new UsersListJSON(users);
        JSONResponseWrapper<UsersListJSON> res = new JSONResponseWrapper<>(uList, globalConfig);
        return res;
    }

    @RequestMapping("/get/{id}")
    public JSONResponseWrapper<UserDataJSON> getUser(@PathVariable("id") long id) {
        return new JSONResponseWrapper<>(
                new UserDataJSON(rep.findOne(id)),
                globalConfig
        );
    }

    @RequestMapping("/get")
    public JSONResponseWrapper<UserDataJSON> getCurentUser() {
        return new JSONResponseWrapper<>(
                new UserDataJSON(
                        (User) SecurityUtils.getCurrentUser()
                ),
                globalConfig
        );
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public JSONResponseWrapper<UserDataJSON> add(
            @RequestParam("name") String name,
            @RequestParam("email") String email,
            @RequestParam("password") String password
    ) throws InvalidPasswordException {
        User res = new User();
        res.setName(name);
        res.setEmail(email);
        res.setPassword(password);
        rep.save(res);
        return new JSONResponseWrapper<>(
                new UserDataJSON(res),
                globalConfig
        );
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or #id == null or #id == principal.getId()")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public JSONResponseWrapper<UserDataJSON> update(
            @RequestParam(value = "id", required = false) Long id,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "newpassword", required = false) String newPassword,
            HttpServletRequest request
    ) throws InvalidPasswordException {
        Long userId = id;
        if (userId == null) {
            userId = SecurityUtils.getCurrentUser().getId();
        }
        User res = rep.findOne(userId);
        if (name != null) {
            res.setName(name);
        }
        //Email может устанавливать только админ
        if (email != null && request.isUserInRole("ROLE_ADMIN")) {
            res.setEmail(email);
        }
        if (newPassword != null) {
            res.setPassword(password, newPassword);
            res.updateSessionKey();
        }
        rep.save(res);
        return new JSONResponseWrapper<>(
                new UserDataJSON(res),
                globalConfig
        );
    }

}
