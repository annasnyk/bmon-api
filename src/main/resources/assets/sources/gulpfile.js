var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
const execFile = require('child_process').execFile;
var fs = require('fs');

var baseDir = '../../'
var migrationsOutputDir = baseDir + 'src/main/resources/db/migration';

function getResultPhpFilePath(sourcePath) {
    var fileName = sourcePath.split(/\/|\\/).pop();
    fileName = fileName.substr(0, fileName.lastIndexOf("."));
    fileName = migrationsOutputDir + '/' + fileName;
    return fileName;
}

function compilePhpTemplate(file) {
    var args = [];
    var inputFile = file.path;
    args.push('-f', inputFile);
    args.push('-c', 'sql/migrations/config/php.ini');
    var outputFile = getResultPhpFilePath(file.path);
    const child = execFile('php', args, function (error, stdout, stderr) {
        if (error) {
            console.log(stdout);
            console.log(stderr);
            throw error;
        }
        fs.writeFile(outputFile, stdout, function (err) {
            if (err) {
                throw err;
            }
        });
    });
}

gulp.task('php-sql', function () {
    return gulp.src('sql/migrations/*.sql.php')
    //.pipe($.cached('php'))
        .pipe($.fn(compilePhpTemplate))
});

gulp.task('default', ['php-sql']);

gulp.task('watch', function () {
    gulp.watch(['sql/migrations/**/*.php'], ['php-sql'])
});