<?php

$scope->comment(
    variables\Versions::V0_0_4,
    'Триггер после добавления новых записей в таблицу пользователей.'
);

$scope->writeAfterWF(
    \helpers\Trigger::beforeEntityChangesTrigger(
        \tables\Apiaries::getInstance(),
        \tables\ApiariesChanges::getInstance()
    ) .
    PHP_EOL . PHP_EOL . PHP_EOL .
    \helpers\Trigger::afterEntityChangesTrigger(
        \tables\Apiaries::getInstance(),
        \tables\ApiariesChanges::getInstance()
    )
);