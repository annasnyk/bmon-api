<?php

namespace helpers;

use helpers\Column as Col;
use parts\Changes;
use tables\Storages;

class ColumnSets
{

    public static function getForEntityChanges($entity)
    {
        $base = [
            Changes::getPkColumn(),
            new Col(
                'local_id',
                'BIGINT',
                [
                    'not_null' => true
                ]
            ),
            new Col(
                'time',
                'TIMESTAMP',
                [
                    'not_null' => true
                ]
            ),
            new Col(
                Storages::getInstance()->singleName . '_id',
                'INTEGER',
                [
                    'not_null' => true
                ]
            ),
            new Col(
                'type',
                'SMALLINT',
                [
                    'not_null' => true
                ]
            )//,
            //Col::getRefColumn($entity)
        ];
        if ($entity->tableExists) {
            array_push(
                $base,
                Col::getRefColumn($entity)
            );
        } else {
            array_push(
                $base,
                Col::getSeqRefColumn($entity)
            );
        }
        $res = array_merge($base, self::getEntityChangesColumns($entity));
        foreach ($entity->entityParams as $column) {
            if (isset($column->options['ref_for'])) {
                $col = $column->options['ref_for']->getLocalIdColumn($column);
                if (isset($col)) {
                    array_push($res, $col);
                }
            }
        }
        array_push($res, new Column(
            $entity->singleName . '_' . Col::LOCAL . '_id',
            'INTEGER',
            [
                'not_null' => true
            ]
        ));
        $res = array_merge($res, self::getEntityChangesConstraints($entity));
        return $res;
    }

    private static function getEntityChangesColumns($entity)
    {
        $params = [];
        foreach ($entity->entityParams as $column) {
            $col = clone $column;
            if (isset($col->options['not_null'])) {
                if ($col->options['not_null']) {
                    $col->options['not_null'] = false;
                }
            }
            array_push($params, $col);
        }
        return $params;
    }

    private static function getEntityChangesConstraints($entity)
    {
        $res = [
            new Column(
                '',
                '',
                [
                    'constraints' => [
                        'UNIQUE (local_id, ' . Storages::getInstance()->singleName . '_id)'
                    ]
                ]
            ),
            new Column(
                '',
                '',
                [
                    'constraints' => [
                        'CHECK (type>=1 AND type<=3)'
                    ]
                ]
            )
        ];
        $i = 0;
        $createStr = '';
        foreach ($entity->entityParams as $column) {
            if (isset($column->options['not_null'])) {
                if ($column->options['not_null']) {
                    if ($i > 0) {
                        $createStr .= ' AND ';
                    }
                    $name = $column->name;
                    if (isset($column->options['ref_for'])) {
                        $col = $column->options['ref_for']->getLocalIdColumn($column);
                        if (isset($col)) {
                            $name = $col->name;
                        }
                    }
                    $createStr .= $name . ' IS NOT NULL';
                    $i++;
                };
            }
        }
        if (strlen($createStr) > 0) {
            array_push($res, new Column(
                '',
                '',
                [
                    'constraints' => ['CHECK (type != 1 OR (' . $createStr . '))']
                ]
            ));
        }
        return $res;
    }

}