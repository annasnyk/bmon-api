<?php

namespace helpers;


use Exception;

class Column
{
    //Имя столбца - строка
    const LOCAL = 'local';
    //Тип - строка
    public $name;
    public $type;
    public $options = [];

    public function __construct($name = '', $type = '', $options = null)
    {
        $this->name = $name;
        $this->type = $type;

        $this->options['not_null'] = false;
        $this->options['constraints'] = [];
        $this->options['ref_for'] = null;
        if (!isset($options)) {
            return;
        }

        if (isset($options['not_null'])) {
            $this->options['not_null'] = $options['not_null'];
        }
        if (isset($options['constraints'])) {
            $this->options['constraints'] = $options['constraints'];
        }
        if (isset($options['ref_for'])) {
            $this->options['ref_for'] = $options['ref_for'];
        }
    }

    public static function getSeqRefColumn($table, $notNull = false, $name = null, $constraints = null)
    {
        $colName = $name === null ? $table->singleName . '_id' : $name;
        $colConstraints = [];
        if (isset($constraints)) {
            $colConstraints = array_merge($colConstraints, $constraints);
        }
        return new Column(
            $colName,
            'INTEGER',
            [
                'not_null' => $notNull,
                'constraints' => $colConstraints,
                'ref_for' => $table
            ]
        );
    }

    public static function getRefColumn($table, $notNull = false, $name = null, $constraints = null)
    {
        $colName = $name === null ? $table->singleName . '_id' : $name;
        $colConstraints = [
            'REFERENCES ' . $table->name . '(id)'
        ];
        if (isset($constraints)) {
            $colConstraints = array_merge($colConstraints, $constraints);
        }
        return new Column(
            $colName,
            'INTEGER',
            [
                'not_null' => $notNull,
                'constraints' => $colConstraints,
                'ref_for' => $table
            ]
        );
    }

    public static function getUniqueConstraint($fields)
    {
        $fieldsStr = '';
        $i = 0;
        foreach ($fields as $field) {
            if ($i++ > 0) {
                $fieldsStr .= ',';
            }
            $fieldsStr .= $field;
        }
        return new Column(
            '',
            '',
            [
                'constraints' => [
                    'UNIQUE(' . $fieldsStr . ')'
                ]
            ]
        );
    }

    public static function getIdColumn($keyType = 'INTEGER')
    {
        $key = (isset($options) && isset($keyType)) ? $keyType : 'INTEGER';
        switch ($key) {
            case 'INTEGER':
                $pkType = 'SERIAL';
                break;
            case 'BIGINT':
                $pkType = 'BIGSERIAL';
                break;
            default:
                throw new Exception('Unknown key type');
        }
        return new Column(
            'id',
            $pkType,
            [
                'constraints' => ['PRIMARY KEY']
            ]
        );
    }

    public static function getDeletedColumn()
    {
        return new Column(
            'deleted',
            'BOOLEAN',
            [
                'constraints' => ['DEFAULT FALSE']
            ]
        );
    }

}