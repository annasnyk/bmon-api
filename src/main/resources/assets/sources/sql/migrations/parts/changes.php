<?php

namespace parts;


use helpers\Column;

class Changes
{
    const NAME = 'changes';
    const COLUMN_NAME = 'change_id';

    public static function getIdColumn($notNull = false, $name = null)
    {
        return new Column(
            isset($name) ? $name : self::COLUMN_NAME,
            'BIGINT',
            [
                'not_null' => $notNull
            ]
        );
    }

    public static function getPkColumn()
    {
        return new Column(
            'id',
            'BIGINT',
            [
                'constraints' => [
                    'PRIMARY KEY',
                    'DEFAULT nextval(\'changes_ids\')'
                ]
            ]
        );
    }
}