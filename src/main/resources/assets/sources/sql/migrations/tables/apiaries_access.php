<?php

namespace tables;

use helpers\Column as Col;
use helpers\Table;

class ApiariesAccess extends Table
{
    public $name = 'apiaries_access';
    public $singleName = 'apiary_access';
    public $hasLocalId = true;
    public $tableExists = true;
    //Представлено ли актуальное состояние сущностей таблицей (true), или же только последовательностью идентификаторов
    protected $columns = [];

    protected function __construct()
    {
        array_push(
            $this->columns,
            Col::getIdColumn(),
            Col::getDeletedColumn()
        );
        $this->entityParams = [
            Col::getRefColumn(Users::getInstance(), true),
            Col::getSeqRefColumn(Apiaries::getInstance(), true),
            new Col(
                'read_only',
                'BOOLEAN',
                [
                    'not_null' => true
                ]
            )
        ];
        $this->columns = $this->columns = array_merge($this->columns, $this->entityParams);
    }

    public function updateAffectedUsersQueries($apiaryAccessId, $changeId)
    {
        $apiaryId = Apiaries::getInstance()->getExternalIdName();
        return Apiaries::getInstance()->updateAffectedUsersQueries('NEW.' . $apiaryId, $changeId);
    }

}