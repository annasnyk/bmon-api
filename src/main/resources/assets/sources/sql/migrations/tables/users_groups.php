<?php

namespace tables;

use helpers\Column as Col;

class UsersGroups extends \helpers\Table
{
    public $name = 'users_groups';
    public $singleName = 'group';
    protected $columns = [];

    protected function __construct()
    {
        array_push(
            $this->columns,
            Col::getIdColumn(),
            Col::getRefColumn(Users::getInstance(), true),
            new Col(
                $this->singleName . '_id',
                'SMALLINT',
                [
                    'not_null' => true
                ]
            ),
            Col::getUniqueConstraint([
                Users::getInstance()->getExternalIdName(),
                $this->getExternalIdName()
            ])
        );
    }
}