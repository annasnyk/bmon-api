<?php
/**
 * Created by IntelliJ IDEA.
 * User: R5
 * Date: 27.07.2016
 * Time: 13:40
 */

namespace tables;

use helpers\Column as Col;
use parts\Changes;

class LinkedUsers extends \helpers\Table
{
    public $name = 'users_linked';
    public $singleName = 'user_linked';
    protected $columns = [];

    protected function __construct()
    {
        array_push(
            $this->columns,
            Col::getIdColumn(),
            Col::getRefColumn(Users::getInstance(), true),
            Col::getRefColumn(Users::getInstance(), true, 'linked_' . Users::getInstance()->getExternalIdName()),
            Changes::getIdColumn(true),
            Col::getUniqueConstraint([
                Users::getInstance()->getExternalIdName(),
                'linked_' . Users::getInstance()->getExternalIdName()
            ])
        );
    }
}