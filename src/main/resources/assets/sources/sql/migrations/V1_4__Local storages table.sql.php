<?php

$scope->comment(
    variables\Versions::V0_0_4,
    'Таблица локальных хранилищ.'
);

$scope->table(\tables\Storages::getInstance());