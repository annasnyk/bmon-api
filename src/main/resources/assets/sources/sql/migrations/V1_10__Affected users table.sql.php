<?php

$scope->comment(
    variables\Versions::V0_0_4,
    'Таблица с информацией о последних изменениях, затронувших конкретных пользователей.'
);

$scope->table(\tables\AffectedUsers::getInstance());