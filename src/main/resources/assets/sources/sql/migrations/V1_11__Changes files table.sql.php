<?php

$scope->comment(
    variables\Versions::V0_0_4,
    'Таблица с информацией о сформированных для клиентских устройств файлах изменений.'
);

$scope->table(\tables\ChangesFiles::getInstance());