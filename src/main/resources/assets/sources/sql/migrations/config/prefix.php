<?php

function camelCaseToSnakeCase($str)
{
    return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $str));
}

function __autoload($class_name)
{
    include dirname(__FILE__) . '\\..\\' . camelCaseToSnakeCase($class_name) . '.php';
}

require_once(dirname(__FILE__) . '\..\libs\SqlFormatter.php');

$content = '';
$contentAfterWF = '';

class Scope
{

    function comment($v, $c)
    {
        global $content;
        $content .= parts\Basic::getHeaderComment($v, $c);
    }

    function table($e)
    {
        global $content;
        $content .= $e->wrapCreateTableColumns(
            $e->getColumns()
        );
    }

    function write($s)
    {
        global $content;
        $content .= $s;
    }

    function writeAfterWF($s)
    {
        global $contentAfterWF;
        $contentAfterWF .= $s;
    }
}

$scope = new Scope();