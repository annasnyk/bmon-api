<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Страница не найдена - BeeMonitor</title>
</head>
<body>
<h1>404</h1>
Page with address <b>${requestPath}</b> was not found.  :(<br>
<a href="${goBackUrl}">&lt; Go back</a>
</body>
</html>