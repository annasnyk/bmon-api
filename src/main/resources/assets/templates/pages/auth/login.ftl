<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Вход в систему - BeeMonitor</title>
</head>
<body>
<form action="${loginUrl}" method="post">
    <label for="email">Email:</label>
    <input type="email" name="email"><br>
    <label for="password">Пароль:</label>
    <input type="password" pattern=".{4,40}" name="password"><br>
    <input type="hidden" name="cookie" value="true">
    <#if redirectUrl??>
        <input type="hidden" name="redirect" value="${redirectUrl}">
    </#if>
    <input type="submit" value="Войти">
</form>
</body>
</html>