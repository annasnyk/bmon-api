<#-- @ftlroot "../" -->
<!DOCTYPE html>
<#include "../properties.ftl">
<head>
    <meta charset="utf-8">
    <title>BeeMonitor</title>
</head>
<body>
<a href="${BASE_URL}">
    <img src="${"${STATIC_URL}/logo_md.png"}"/>
</a>
<h1>BeeMonitor - сервис для ведения пчеловодных журналов</h1>
<p>
<#if currentUser??>
    <i>Здравствуйте, ${currentUser.getName()}!</i><br>
<form action="${"${BASE_URL}/auth/logout"}" method="post">
    <input type="hidden" name="redirect" value="${BASE_URL}">
    <input type="submit" value="Выход из системы">
</form>
<#else>
<a href="${"${BASE_URL}/auth/login"}">Вход в систему</a>
</#if>
</p>
<#if currentUser??>
<p>
<ol>
    <li><a href="${"${BASE_URL}/user/list"}">Список пользователей</a></li>
</ol>
</p>
</#if>
</body>
</html>