/**
v0.0.4
Таблица изменений сущностей типа Пасека.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE TABLE apiaries_changes (
  id              BIGINT PRIMARY KEY DEFAULT nextval('changes_ids'),
  local_id        BIGINT    NOT NULL,
  time            TIMESTAMP NOT NULL,
  storage_id      INTEGER   NOT NULL,
  type            SMALLINT  NOT NULL,
  apiary_id       INTEGER,
  name            VARCHAR(128),
  description     VARCHAR(512),
  apiary_local_id INTEGER   NOT NULL,
  UNIQUE (local_id, storage_id),
  CHECK (
    type >= 1
    AND type <= 3
  ),
  CHECK (
    type != 1
    OR (name IS NOT NULL)
  )
);
