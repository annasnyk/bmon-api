/**
v0.0.4
Таблица изменений сущностей типа: Доступ к объекту типа Пасека.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE TABLE apiaries_access_changes (
  id                     BIGINT PRIMARY KEY DEFAULT nextval('changes_ids'),
  local_id               BIGINT    NOT NULL,
  time                   TIMESTAMP NOT NULL,
  storage_id             INTEGER   NOT NULL,
  type                   SMALLINT  NOT NULL,
  apiary_access_id       INTEGER REFERENCES apiaries_access (id),
  user_id                INTEGER REFERENCES users (id),
  apiary_id              INTEGER,
  read_only              BOOLEAN,
  apiary_local_id        INTEGER   NOT NULL,
  apiary_access_local_id INTEGER   NOT NULL,
  UNIQUE (local_id, storage_id),
  CHECK (
    type >= 1
    AND type <= 3
  ),
  CHECK (
    type != 1
    OR (
      user_id IS NOT NULL
      AND apiary_local_id IS NOT NULL
      AND read_only IS NOT NULL
    )
  )
);
