/**
v0.0.2
Таблица с правами доступа пользователей к пасекам.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE TABLE apiaries_access (
  id        SERIAL PRIMARY KEY,
  deleted   BOOLEAN DEFAULT FALSE,
  user_id   INTEGER REFERENCES users (id) NOT NULL,
  apiary_id INTEGER                       NOT NULL,
  read_only BOOLEAN                       NOT NULL
);
