/**
v0.0.4
Триггер после добавления новых записей в таблицу пользователей.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE FUNCTION TRFN_AfterUsersInsert()
  RETURNS TRIGGER AS $$
BEGIN
  INSERT INTO users_affected (user_id, change_id) VALUES (NEW.id, 0);
  INSERT INTO changes_files (user_id, start_id, end_id) VALUES (NEW.id, 0, 0);
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TR_AfterUsersInsert
AFTER INSERT ON users
FOR EACH ROW
EXECUTE PROCEDURE TRFN_AfterUsersInsert();