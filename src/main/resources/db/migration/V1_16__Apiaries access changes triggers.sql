/**
v0.0.4
Триггер после добавления новых записей в таблицу пользователей.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE FUNCTION TRFN_BeforeApiariesAccessChangesInsert()
  RETURNS TRIGGER AS $$
BEGIN

  IF NEW.apiary_id IS NULL
  THEN
    NEW.apiary_id = (
      SELECT apiary_id
      FROM apiaries_changes
      WHERE storage_id = NEW.storage_id AND apiary_local_id = NEW.apiary_local_id AND type = 1
    );
  END IF;
  IF NEW.type = 1
  THEN
    -- Create global entity ID when change has 'create' type
    INSERT INTO apiaries_access (user_id, apiary_id, read_only) VALUES (NEW.user_id, NEW.apiary_id, NEW.read_only)
    RETURNING id
      INTO NEW.apiary_access_id;
  ELSE
    -- Get global entity IDs if not set

    IF NEW.apiary_access_id IS NULL
    THEN
      NEW.apiary_access_id = (
        SELECT apiary_access_id
        FROM apiaries_access_changes
        WHERE storage_id = NEW.storage_id AND apiary_access_local_id = NEW.apiary_access_local_id AND type = 1
      );
    END IF;
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TR_BeforeApiariesAccessChangesInsert
BEFORE INSERT ON apiaries_access_changes
FOR EACH ROW
EXECUTE PROCEDURE TRFN_BeforeApiariesAccessChangesInsert();


CREATE FUNCTION TRFN_AfterApiariesAccessChangesInsert()
  RETURNS TRIGGER AS $$
DECLARE
  parameter_last_update TIMESTAMP;
  rw                    INTEGER;
BEGIN
  -- Manipulating entity table
  IF NEW.type = 3
  THEN
    UPDATE apiaries_access
    SET deleted = TRUE
    WHERE id = NEW.apiary_access_id;
  ELSIF NEW.type = 2
    THEN

      IF NEW.user_id IS NOT NULL
      THEN
        parameter_last_update := (SELECT time
                                  FROM apiaries_access_changes
                                  WHERE apiary_access_id = NEW.apiary_access_id AND id != NEW.id AND user_id IS NOT NULL
                                  ORDER BY time DESC
                                  LIMIT 1);
        IF parameter_last_update IS NOT NULL AND parameter_last_update < NEW.time
        THEN
          UPDATE apiaries_access
          SET user_id = NEW.user_id
          WHERE id = NEW.apiary_access_id;
        END IF;
      END IF;
      IF NEW.apiary_id IS NOT NULL
      THEN
        parameter_last_update := (SELECT time
                                  FROM apiaries_access_changes
                                  WHERE
                                    apiary_access_id = NEW.apiary_access_id AND id != NEW.id AND apiary_id IS NOT NULL
                                  ORDER BY time DESC
                                  LIMIT 1);
        IF parameter_last_update IS NOT NULL AND parameter_last_update < NEW.time
        THEN
          UPDATE apiaries_access
          SET apiary_id = NEW.apiary_id
          WHERE id = NEW.apiary_access_id;
        END IF;
      END IF;
      IF NEW.read_only IS NOT NULL
      THEN
        parameter_last_update := (SELECT time
                                  FROM apiaries_access_changes
                                  WHERE
                                    apiary_access_id = NEW.apiary_access_id AND id != NEW.id AND read_only IS NOT NULL
                                  ORDER BY time DESC
                                  LIMIT 1);
        IF parameter_last_update IS NOT NULL AND parameter_last_update < NEW.time
        THEN
          UPDATE apiaries_access
          SET read_only = NEW.read_only
          WHERE id = NEW.apiary_access_id;
        END IF;
      END IF;
  END IF;
  -- Adding linked users
  IF NEW.type = 1
  THEN
    IF NEW.read_only = FALSE
    THEN
      FOR rw IN SELECT user_id
                FROM apiaries_access_changes
                WHERE apiary_id = NEW.apiary_id AND user_id != NEW.user_id
      LOOP
        INSERT INTO users_linked (user_id, linked_user_id, change_id) VALUES (rw, NEW.user_id, NEW.id)
        ON CONFLICT DO NOTHING;
      END LOOP;
    END IF;
    FOR rw IN SELECT user_id
              FROM apiaries_access_changes
              WHERE apiary_id = NEW.apiary_id AND user_id != NEW.user_id
    LOOP
      INSERT INTO users_linked (user_id, linked_user_id, change_id) VALUES (NEW.user_id, rw, NEW.id)
      ON CONFLICT DO NOTHING;
    END LOOP;
  END IF;
  -- Marking affected users
  FOR rw IN SELECT user_id
            FROM apiaries_access
            WHERE apiary_id = NEW.apiary_id
  LOOP
    UPDATE users_affected
    SET change_id = NEW.id
    WHERE user_id = rw;
  END LOOP;
  -- Setting last change parameter value
  UPDATE parameters_bigint
  SET value = NEW.id
  WHERE name = 'last_change';
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TR_AfterApiariesAccessChangesInsert
AFTER INSERT ON apiaries_access_changes
FOR EACH ROW
EXECUTE PROCEDURE TRFN_AfterApiariesAccessChangesInsert();